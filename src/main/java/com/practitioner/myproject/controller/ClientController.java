package com.practitioner.myproject.controller;

import com.practitioner.myproject.model.Client;
import com.practitioner.myproject.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Routes.CLIENTS)
public class ClientController {

    @Autowired
    ClientService clientService;

    @GetMapping()
    public CollectionModel<EntityModel<Client>> listClients(@RequestParam int page, @RequestParam int size) {
        final List<Client> client = this.clientService.listClients(page - 1, size);
        final List<EntityModel<Client>> clients = client.stream().map(i ->
                EntityModel.of(i).add(linkTo(methodOn(this.getClass()).getClient(i.document)).withSelfRel())
                ).collect(Collectors.toList());
        final Link link = linkTo(methodOn(this.getClass()).listClients(1,10)).withSelfRel().withTitle("Clients");
        return CollectionModel.of(clients).add(link);
    }

    @PostMapping()
    public ResponseEntity insertClient(@RequestBody Client client) {
        this.clientService.insertClient(client);
        final Link clientURL = linkTo(methodOn(ClientController.class).getClient(client.document)).withSelfRel();
        return ResponseEntity.ok().location(clientURL.toUri()).build();
    }

    @GetMapping("/{doc}")
    public EntityModel<Client> getClient(@PathVariable String doc) {
        try {
            final Link link = linkTo(methodOn(this.getClass()).getClient(doc)).withSelfRel();
            final Link link2 = linkTo(methodOn(this.getClass()).listClients(1,10)).withRel("clients").withTitle("Clients");
            final Link link3 = linkTo(methodOn(ClientAccountsController.class).listClientAccounts(doc)).withRel("clientAccounts").withTitle("Client Accounts");
            final Client client = this.clientService.getClient(doc);
            return EntityModel.of(client).add(link, link2, link3);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{doc}")
    public void updateClient(@PathVariable String doc, @RequestBody Client client) {
        try {
            client.document = doc;
            this.clientService.updateClient(client);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{doc}")
    public void patchClient(@PathVariable String doc, @RequestBody Client client) {
        client.document = doc;
        this.clientService.patchClient(client);
    }

    @DeleteMapping("/{doc}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void  deleteClient(@PathVariable String doc) {
        try {
            this.clientService.deleteClient(doc);
        } catch (Exception e) {
        }
    }
}
