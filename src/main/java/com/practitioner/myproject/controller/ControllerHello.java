package com.practitioner.myproject.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello/v1")
public class ControllerHello {

    @GetMapping
    public String saludar() {
        return "Hello!!";
    }

    @PutMapping
    public void modifySaludo(@RequestBody String saludo) {
        System.out.println(saludo);
    }
}
