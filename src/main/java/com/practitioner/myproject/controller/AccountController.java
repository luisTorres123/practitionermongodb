package com.practitioner.myproject.controller;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Routes.ACCOUNTS)
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping()
    public List<Account> listAcoounts() {
        return this.accountService.listAccounts();
    }

    @PostMapping()
    public void insertAccount(@RequestBody Account account) {
        this.accountService.insertAccount(account);
    }

    @GetMapping("/{number}")
    public Account getAccount(@PathVariable String number) {
        try {
            return this.accountService.getAccount(number);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{number}")
    public void updateAccount(@PathVariable String number, @RequestBody Account account) {
        try {
            account.number = number;
            this.accountService.updateAccount(account);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{number}")
    public void patchAccount(@PathVariable String number, @RequestBody Account account) {
        account.number = number;
        this.accountService.patchAccount(account);
    }

    @DeleteMapping("/{number}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void  deleteAccount(@PathVariable String number) {
        try {
            this.accountService.deleteAccount(number);
        } catch (Exception e) {
        }
    }
}
