package com.practitioner.myproject.controller;

public class Routes {
    public static final String BASE = "/api/v1";
    public static final String CLIENTS = BASE + "/clients";
    public static final String ACCOUNTS = BASE + "/accounts";
}
