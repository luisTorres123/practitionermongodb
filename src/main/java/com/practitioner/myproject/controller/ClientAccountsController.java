package com.practitioner.myproject.controller;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.service.AccountService;
import com.practitioner.myproject.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Routes.CLIENTS + "/{document}/accounts")
public class ClientAccountsController {

    @Autowired
    ClientService clientService;

    @Autowired
    AccountService accountService;

    public static class dataAccount {
        public String code;
    }

    @PostMapping()
    public ResponseEntity insertClientAccount(@PathVariable String document, @RequestBody dataAccount data) {
        try {
            this.clientService.insertClientAccount(document, data.code);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping()
    public ResponseEntity<List<String>> listClientAccounts(@PathVariable String document) {
        try {
            return ResponseEntity.ok(this.clientService.listClientAccounts(document));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{acc}")
    public  ResponseEntity<Account> getClientAccount(@PathVariable String document, @PathVariable String acc) {
        try {
            if (this.clientService.IsClientAccount(document, acc))
                return ResponseEntity.ok(this.accountService.getAccount(acc));
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{acc}")
    public  ResponseEntity deleteClientAccount(@PathVariable String document, @PathVariable String acc) {
        try {
            if (this.clientService.IsClientAccount(document, acc)) {
                this.accountService.getAccount(acc).status = "INACTIVA";
                this.clientService.getClient(document).accounts.remove(acc);
            }
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
