package com.practitioner.myproject.service;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.model.Client;

import java.util.List;

public interface ClientService {
    public void insertClient(Client client);
    public List<Client> listClients(int page, int size);
    public Client getClient(String doc);
    public void updateClient(Client client);
    public void patchClient(Client client);
    public void deleteClient(String doc);
    public void insertClientAccount(String document, String number_account);
    public List<String> listClientAccounts(String document);
    public boolean IsClientAccount(String doc, String acc);
}
