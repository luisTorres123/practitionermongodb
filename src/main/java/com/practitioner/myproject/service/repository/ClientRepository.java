package com.practitioner.myproject.service.repository;

import com.practitioner.myproject.model.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientRepository extends MongoRepository<Client, String> {

}
