package com.practitioner.myproject.service.repository;

import com.practitioner.myproject.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account, String> {

}
