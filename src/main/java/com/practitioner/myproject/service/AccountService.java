package com.practitioner.myproject.service;

import com.practitioner.myproject.model.Account;

import java.util.List;

public interface AccountService {
    public void insertAccount(Account account);
    public List<Account> listAccounts();
    public Account getAccount(String number);
    public void updateAccount(Account account);
    public void patchAccount(Account account);
    public void deleteAccount(String number);
}
