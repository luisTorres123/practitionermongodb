package com.practitioner.myproject.service.impl;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.model.Client;
import com.practitioner.myproject.service.AccountService;
import com.practitioner.myproject.service.ClientService;
import com.practitioner.myproject.service.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Override
    public void insertClient(Client client) {
        this.clientRepository.insert(client);
    }

    @Override
    public List<Client> listClients(int page, int size) {
        return this.clientRepository.findAll();
    }

    @Override
    public Client getClient(String doc) {
        final Optional<Client> client = this.clientRepository.findById(doc);
        return client.isPresent() ? client.get() : null;
    }

    @Override
    public void updateClient(Client client) {
        this.clientRepository.save(client);
    }

    @Override
    public void patchClient(Client client) {

    }

    @Override
    public void deleteClient(String doc) {
        this.clientRepository.deleteById(doc);
    }

    @Override
    public void insertClientAccount(String document, String number_account) {
        final Client client = this.getClient(document);
        client.accounts.add(number_account);
        this.clientRepository.save(client);
    }

    @Override
    public List<String> listClientAccounts(String document) {
        final Client client = this.getClient(document);
        return client.accounts;
    }

    @Override
    public boolean IsClientAccount(String doc, String acc) {
        return this.getClient(doc).accounts.contains(acc);
    }
}
