package com.practitioner.myproject.service.impl;

import com.practitioner.myproject.model.Account;
import com.practitioner.myproject.service.AccountService;
import com.practitioner.myproject.service.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public void insertAccount(Account account) {
        this.accountRepository.insert(account);
    }

    @Override
    public List<Account> listAccounts() {
        return this.accountRepository.findAll();
    }

    @Override
    public Account getAccount(String number) {
        final Optional<Account> acc = this.accountRepository.findById(number);
        return acc.isPresent() ? acc.get() : null;
    }

    @Override
    public void updateAccount(Account account) {
        this.accountRepository.save(account);
    }

    @Override
    public void patchAccount(Account account) {

    }

    @Override
    public void deleteAccount(String number) {
        this.accountRepository.deleteById(number);
    }
}
