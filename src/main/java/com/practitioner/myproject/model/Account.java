package com.practitioner.myproject.model;

import org.springframework.data.annotation.Id;

public class Account {
    @Id public String number;
    public String currency;
    public Double amount;
    public String type;
    public String status;
    public String office;
}
