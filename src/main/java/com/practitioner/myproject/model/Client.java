package com.practitioner.myproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Client {
    @Id public String document;
    public String name;
    public int age;
    public String phone;
    public String email;
    public String address;

    @JsonIgnore
    public List<String> accounts = new ArrayList<>();
}
